After discussion with Peter it seems that teh way to do things is to set up two servers in the Amazon cloud, and to deploy the ansible-infrastructure machines there.

By creating the machines we are bypassing the vagrant steps.

The settings for thee machines should be dne via ansible - running the playbooks provided by Peter in the vagrant workflow.

I have set up an account with padraic.stack@nuim.ie, it has $75 credit. Probably best to talk to Hugh about getting a cc sanctioned for a new account with a different email address.

Machines:

- i-fb854543: ec2-52-18-56-225.eu-west-1.compute.amazonaws.com ; 52.18.56.255
- i-f8854540: ec2-52-30-2-84.eu-west-1.compute.amazonaws.com

I also have a 10gb network volume set up, not live yet because I don't know what to do with it.

I have created an S3 bucket called 'mudlbucket'.
- http://mudlbucket.s3-website-eu-west-1.amazonaws.com/

I don't know how to pass credentials of it to anything else yet.
