# git commands

To test access to repos:

~~~bash
$ ssh git@repo.url.ie
~~~

Output should be along line of 

~~~bash

hello pstack, this is gitolite 2.3-1 (Debian) running on git 1.9.1
the gitolite config gives you the following access:
     R          drirepo/dri-app
     R          drirepo/dri-cli
     R          drirepo/dri-data-models
     R   W      drirepo/dri-docs
     R          drirepo/dri-user-group
     R          drirepo/dri-workflows-tests
     R          drirepo/hydra-jetty 
~~~


To clone a repo

~~~bash
$ git clone git@tracker.dri.ie:drirepo/dri-app.git
~~~

To check branches on a repo

~~~bash
$ git branch 
~~~

To switch branches

~~~bash
$ git checkout -b branch name
~~~

To check repo status

~~~bash
$ git status
~~~

To add all files / folders to the index, ready to commit or do whatever with
~~~bash
$ git add -A 
~~~

To commit [with a message]

~~~bash
$ git commin -m "your commit message"
~~~

To show git log 

~~~bash
$ git log 
~~~

To fetch from and merge updates from source

~~~bash
$ git pull
~~~ 

# Steps taken to switch to development branch from master branch

To check git status

~~~bash
mudri@lb-srv-5hz:~/dri-app$ git status
On branch master
Your branch is up-to-date with 'origin/master'.
~~~

### do local commit
~~~bash
mudri@lb-srv-5hz:~/dri-app$ git commit -a -m "local commit before switching to dev branch"
~~~
### check all remote  branch
~~~bash
mudri@lb-srv-5hz:~/dri-app$ git branch -r
origin/HEAD -> origin/master
  origin/december_uat
  origin/develop
  origin/feature/jruby
  origin/feb_ui
  origin/linked
  origin/marc-nccb
  origin/master
  origin/october_ingest
  origin/release-14.12.1
  origin/release-15.02.0
  origin/release-15.02.1
  origin/release-15.02.2
  origin/release-15.02.3
~~~

## switch to the development branch

~~~bash
mudri@lb-srv-5hz:~/dri-app$ git checkout -b develop
~~~

### pull the latest codes from development branch

~~~bash
mudri@lb-srv-5hz:~/dri-app$ git pull origin develop
~~~

### git status to show the latest status

~~~bash
mudri@lb-srv-5hz:~/dri-app$ git status
~~~

### local commit again

~~~bash
mudri@lb-srv-5hz:~/dri-app$  git commit -a -m "local commit after switching to dev branch"
~~~

### git add to bring in the untracked files:

~~~bash
mudri@lb-srv-5hz:~/dri-app$ git add FILENAME
~~~

[ ```git add .``` will add everything in the folder which Agustina advises against - it's better to add things by name] 

### one more commit

~~~bash
mudri@lb-srv-5hz:~/dri-app$ git commit -m "commit to bring old files"
~~~

### reset to a previous commit

this will get rid of everything you've done since then:

~~~bash
git reset --hard COMMITNUMBER
~~~

There are other options if you want to reset but keep some work whether published in commits or not.

## Switch git origin
Remove existing origin

~~~bash
git remote remove origin
~~~

add new one [i.e. gitlab -needs to be created in gitlab first]

~~~bash

git remote add origin git@gitlab.com:padraic7a/mulocal.git
~~~
