#! /bin/bash
#Create an array of the PDF files in the current directory.

files=(`ls -1 *.pdf`)

# Iterate for each PDF file in the array
for file in ${files[@]}
do
  image_root=`echo $file |sed -e "s/\.pdf//"`
  /usr/bin/pdfimages -j $file $image_root
done

exit 0
