added line 49 to make the timeline event label text black:

.neatlinetime-timeline .timeline-event-label {
 47     /*background: rgba(0,0,0, 0.05);*/
 48     font-size: 14px !important;
 49     color:black;
 50 }


added line 124 to /var/www/html/plugins/Neatlinetime/views/shared/css to make the text inside the timeline bubbles black

/* The event description inside an event bubble. */
120 .timeline-event-bubble-body {
121     font-size: 12px !important;
122     border-width: 1px 0 !important;
123     padding:10px 20px !important;
124     color: black;
125 }



