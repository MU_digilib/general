# Error Checking

Some records seem to have a truncated description field. May be from initial import of records into Omeka.

### Affected collections

- published works
- newspaper clippings
- drafts and scripts


