# General 

A place to store files not tied to any particular project that might be of use or worth sharing.

## .bashrc

The .bashrc file our Hydra server.
It has some useful settings for bash history and the prompt displays the git branch you are currently in.

## git Commands

A short list and explanation of the git commands we use for future reference.


## dri-app

Files related to our implementation of the dri-app [developer installation]

## shell scripts 

Handy scripts for working on things.
