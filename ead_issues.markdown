Encoding needs to be in utf-8, not iso 

dates: 

<ead:unitdate datechar="creation"
                       encodinganalog="3.1.3.2"
                       type="inclusive"
                       normal="1992-2006"

should be a foward slash, not a hyphen

line 12:
      <ead:eadid countrycode="IE" mainagencycode="MU" identifier="IE-MU-">IE-MU-</ead:eadid>
should be
<ead:eadid countrycode="IE" mainagencycode="IE-MU" identifier="IE-MU-">IE-MU-</ead:eadid>

line 25
            <ead:language scriptcode="Lat" langcode="eng">English</ead:language>
should be
<ead:language scriptcode="Latn" langcode="eng">English</ead:language>


unit_id needs repository code and unit code


There's another issue, where identifier should be taken from the element value, see below. I should write a transform for this, check with Agustina if having trouble.

<ead:unitid encodinganalog="3.1.1"
                     label="Reference"
                     countrycode="IE" repositorycode="IE-MU" identifier="IE-MU-">PP7</ead:unitid>
      </ead:did>


