Check status of drive, is it ours or do we have to return it to Glenbeigh?

Are we moving contents to another drive? If so check space restrictions and batch drive contents as required. Keep folders of jpegs with corresponding folders of tiffs. 

Use Australian National Archives 'Manifest Maker' software [Windows] to create a 'manifest.tsv' of the folder.

Move folder from one disc to another.

Create another tsv on the destination drive- name the collection, folder and date. 

Compare / diff with the manifest to check for changes or errors when copying files.

Leave a copy of each manifest in the appropiate folder of the storage device and in the correct 'C:\Scanned Collections' folder of my library based pc.

Update the bxlist.txt file on each storage device.





