Enter mysql console:

~~~mysql
mysql
~~~

list databases [works on computer wide level], however it won't show root databases unless you're logged in as root.

~~~mysql
show databases;
~~~

work with a database
~~~mysql
use DATABASE;
~~~

show tables 
~~~mysql
show tables;
~~~

show columns
~~~mysql
describe TABLE;

+------------------------+-------------+------+-----+---------+-------+
| Field                  | Type        | Null | Key | Default | Extra |
+------------------------+-------------+------+-----+---------+-------+
| PLUGIN_NAME            | varchar(64) | NO   |     |         |       |
| PLUGIN_VERSION         | varchar(20) | NO   |     |         |       |
| PLUGIN_STATUS          | varchar(10) | NO   |     |         |       |
| PLUGIN_TYPE            | varchar(80) | NO   |     |         |       |
| PLUGIN_TYPE_VERSION    | varchar(20) | NO   |     |         |       |
| PLUGIN_LIBRARY         | varchar(64) | YES  |     | NULL    |       |
| PLUGIN_LIBRARY_VERSION | varchar(20) | YES  |     | NULL    |       |
| PLUGIN_AUTHOR          | varchar(64) | YES  |     | NULL    |       |
| PLUGIN_DESCRIPTION     | longtext    | YES  |     | NULL    |       |
| PLUGIN_LICENSE         | varchar(80) | YES  |     | NULL    |       |
+------------------------+-------------+------+-----+---------+-------+
10 rows in set (0.00 sec)

~~~

show all rows
~~~mysql
select * from TABLE;
~~~

Delete record
~~~mysql
delete from TABLE where id=10;

~~~
