Welcome.
The Digital Library is the digital repository for both Maynooth University and St. Patrick's College Maynooth. On this site, you can browse and search both contemporary and historical collections from one location. We hope you enjoy your visit. 
