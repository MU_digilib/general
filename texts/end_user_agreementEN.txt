End User Agreement

DRAFT ONLY 

    Ownership and copyright information for the digital objects are stated explicitly in the Rights statement of each metadata record.
    You agree to use the digital objects only in accordance with this End User Agreement. You agree to notify the Digital Library of any breach of its terms or of any infringements of the digital objects of which you become aware as quickly as possible.
    You will abide by the appropriate copyright and licence statements applied to digital object and metadata.
    You will ensure that means of access to data are kept secure and used only for appropriate purposes. In particular, passwords are personal and should not be shared.
    Whenever you use a digital object you should, where possible, use the bibliographic citation recommended by the Digital Library, or an equivalent.
    Use of the data on this web site is at your sole risk. You agree not to use this site for any illegal or unlawful purpose. In particular, you will not use the digital objects or metadata in a manner which infringes the law relating to copyright, confidentiality, privacy, data protection, defamation or similar or related doctrines.
    The Digital Library excludes, to the maximum extent permitted by law, all express or implied warranties of any kind in relation to any digital object or metadata; in particular, the Digital Library shall not be liable for any loss or damage (i) which may be suffered or incurred by you or a third party in respect of the use by you of any digital object or metadata, or (ii) which may arise directly or indirectly in respect of the use by you of any digital object or metadata.
    You will offer for deposit any new digital objects which have been derived from the digital objects supplied.
    Any breach of this End User Agreement will lead to the immediate and automatic termination without notice of your access to the services, and could result in legal action against you.
    This Agreement shall be governed by and construed in accordance with the laws of the Republic of Ireland and each Party irrevocably submits to the exclusive jurisdiction of the courts of the Republic of Ireland over any claim or matter arising under or in connection with this Agreement.
