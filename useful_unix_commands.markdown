Grep for a string in a file, -r means resursively, -n shows the line number.
2>/dev/null means that all error messages [stderr output] about file permissions and invalid arguments get sent to /dev/null instead of being printed to the terminal. Useful for when searching computer wide - but not when you're not sure if you're grepping correctly. 	
~~~bash
grep -rn "string" 2>/dev/null

~~~

To rename all .pdf files in a folder, changing the '-' to '_'. 
~~~bash
rename 's/\:/-/g' *.pdf -vn

~~~

This will preview the new file names, if they look ok run the command again, omitting the 'n'.

~~~bash


~~~

~~~bash


~~~

~~~bash


~~~

~~~bash


~~~


