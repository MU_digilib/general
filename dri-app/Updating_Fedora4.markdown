# Updating the DRI-app to Fedora 4

It should be fairly straightforward. After chatting to Agustina it will involve the following steps:

- resetting jetty [removing and recreating]
- reseting fakes3
- pulling in new code
- usual merge

I'm trying on my laptop first. 

- check everything works as it should
- stop relevant services
- delete jetty folder, fakes3_root folder
- git pull
- git submodule inint
- git submodule update

this times out, asking Agustina what the story is, waiting for a response 

Didn't figure out what is going on here at all. It's not a problem for Agustina. Here's the email I sent Peter:

Hi Peter,

I am attempting to update my local version of the develop branch.

'git pull' seems to works find but I am having problems with jetty

On my laptop I just cannot update the submodule, it times out.

I get:

    padraic@laptop:~/dri-app$ git submodule update --init --recursive
    Submodule 'jetty' (ssh://git@tracker.dri.ie:2200/drirepo/hydra-jetty) registered for path 'jetty'
    ssh: connect to host tracker.dri.ie port 22: Connection timed out
    fatal: The remote end hung up unexpectedly
    Unable to fetch in submodule path 'jetty'


However I can clone the hydra-jetty repo on this machine without any problems.

If I do a fresh clone of the dri-app then 'git submodule update --init --recursive' works fine.

Is this some kind of permissions problem?


I know it's foolish but I decided to try on our server.


    mudri@lb-srv-5hz:~/dri-app$ git pull
    key_read: uudecode AAAAB3NzaC1yc2EAAAADAQABAAABAQCtkCVkezswZEDXHDJrhLep374eUGXU442oyNKCPfHBm7hrLDwiloZ4XII+xPyojdC8NxXFzdL+bHUD6sVgcy+DtrMUFq9+KZ+dR+GKl+FaJRIxvEdnoH0NJopjUSRPZpbjCVj6UhJzIWoTtb3O10EO8xQDvBEJ9UDXDyuvTks
     failed
    remote: Counting objects: 1272, done.
    remote: Compressing objects: 100% (850/850), done.
    remote: Total 869 (delta 635), reused 1 (delta 0)
    Receiving objects: 100% (869/869), 357.43 KiB | 0 bytes/s, done.
    Resolving deltas: 100% (635/635), completed with 89 local objects.
    From ssh://tracker.dri.ie:2200/drirepo/dri-app
       7f6cc0e..26f2e1f  develop    -> origin/develop
     * [new branch]      doi_updates -> origin/doi_updates
       d5e583a..1c902a8  hydra9     -> origin/hydra9
       b9b2a91..2c7c5ec  marc-nccb  -> origin/marc-nccb
       42e4ec8..da7364d  master     -> origin/master
     * [new branch]      release-15.06.0 -> origin/release-15.06.0
     * [new branch]      release-15.06.1 -> origin/release-15.06.1
    Fetching submodule jetty
    ssh: connect to host tracker.dri.ie port 22: Connection timed out
    fatal: Could not read from remote repository.

    Please make sure you have the correct access rights
    and the repository exists.


Is this a manifestation of the same problem?


All the best,

Padraic


===

July 31st

So, that was an issue with the old repo address hiding in a submodule folder. Next time I get an issue like this I could try grepping for the address stem and seeing where it shows up.

===

Having a problem now with sprocket and versions clashing in the Gemfile and Gemfile.lock. git response to 'bundle' is to use 'bundle update' whihc Agustina has advised against.
